## Solution for the Juggle Fest test code##

**C# application for the Juggle Fest problem by** [Yodle](http://www.yodlecareers.com/puzzles/jugglefest.html).

### Start ###

After loading juggles and circuits data to Dictionaries the application creates a list of the jugglers skill (`skillist`) ordered by:

- preference position
- match score for that circuit
- juggler id

### Processing ###

The first juggler in the list is assigned to the circuit and the remaining items for that juggler were removed.
If the circuit is full all items in the `skillist` for that circuit are removed, and the other items for that juggler with following preference are shifted up.

### Finally ###

Just loop to the circuits list to create the output file and get the specific circuit.