﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Collections;
using System.Diagnostics;

namespace JuggleFest
{
    class Program
    {
        static void Main(string[] args)
        {
            int JugglersCount = 0;
            var AppRoot = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName;

            //reading from static file
            var textFromFile = File.ReadAllText(AppRoot + "\\jugglefest.txt");

            //reading file from web
            //var url = "http://www.yodlecareers.com/puzzles/jugglefest.txt";
            //var textFromFile = (new WebClient()).DownloadString(url);

            string jugglerid;

            Dictionary<string, Circuit> circuits = new Dictionary<string, Circuit>();
            Dictionary<string, Juggler> jugglers = new Dictionary<string, Juggler>();

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            //main list ordered by position, skill, jugglerid 
            //don't care about the value of SortedDictionary, only the key is important
            //using SortedSet in place of SortedDictionary leads to worst performance
            var skillist = new SortedDictionary<JugglerSkill, JugglerSkill>
                        (Comparer<JugglerSkill>.Create((x, y) =>
                         {
                             int result = x.Position.CompareTo(y.Position);
                             if (result == 0)
                                 result = y.Skill.CompareTo(x.Skill);
                             if (result == 0)
                                 result = x.JugglerId.CompareTo(y.JugglerId);
                             return result;
                         }
                        ));

            using (StringReader reader = new StringReader(textFromFile))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var array = line.TrimEnd().Split(' ');

                    if (array[0] == "C")
                        circuits.Add(array[1], new Circuit
                        {
                            CircuitId = array[1],
                            H = int.Parse(array[2].Substring(2)),
                            E = int.Parse(array[3].Substring(2)),
                            P = int.Parse(array[4].Substring(2)),
                            jugglers = new List<Juggler>()
                        });
                    else if (array[0] == "J")
                    {
                        JugglersCount++;
                        var prefcircuits = new List<Circuit>(Array.ConvertAll(array[5].Split(','), element => circuits[element]));

                        Juggler juggler = new Juggler
                        {
                            JugglerId = array[1],
                            H = int.Parse(array[2].Substring(2)),
                            E = int.Parse(array[3].Substring(2)),
                            P = int.Parse(array[4].Substring(2)),
                            PreferredCircuits = new List<JugglerSkill>()
                        };
                        jugglers.Add(juggler.JugglerId, juggler);

                        int position = 0;
                        foreach (var preferredcircuit in prefcircuits)
                        {
                            position++;
                            var skill = preferredcircuit.H * juggler.H + preferredcircuit.E * juggler.E + preferredcircuit.P * juggler.P;
                            var jugglerskill = new JugglerSkill
                            {
                                JugglerId = juggler.JugglerId,
                                Position = position,
                                CircuitId = preferredcircuit.CircuitId,
                                Skill = skill,
                            };
                            juggler.PreferredCircuits.Add(jugglerskill);
                            skillist.Add(jugglerskill, jugglerskill);
                        }
                    }
                }

                int jugglerpercircuit = JugglersCount / circuits.Count;

                while (skillist.Count() > 0)
                {
                    var item = skillist.First();
                    jugglerid = item.Key.JugglerId;
                    //add the top juggler to the circuit
                    circuits[item.Key.CircuitId].jugglers.Add(jugglers[jugglerid]);

                    //remove the remaining skill items for that juggler
                    var toremove = skillist.Where(x => x.Key.JugglerId == jugglerid).Select(x => x.Key).ToList();
                    foreach (var ri in toremove)
                    {
                        skillist.Remove(ri);
                    }

                    //when the circuit is full delete remaining items for that circuit
                    if (circuits[item.Value.CircuitId].jugglers.Count == jugglerpercircuit)
                    {
                        var itemstoremove = skillist.Where(x => x.Key.CircuitId == item.Key.CircuitId).Select(x => x.Key).ToList();
                        foreach (var itemtoremove in itemstoremove)
                        {
                            //delete the item    
                            skillist.Remove(itemtoremove);
                            //shift remaining items for that juggler to upper position
                            var itemstoupdate = skillist.Where(x => x.Key.JugglerId == itemtoremove.JugglerId && x.Value.Position > itemtoremove.Position).Select(x => x.Key).ToList();
                            foreach (var itemtoupdate in itemstoupdate)
                            {
                                //Changing the sort values of existing items is not supported and may lead to unexpected behavior
                                skillist.Remove(itemtoupdate);
                                itemtoupdate.Position = itemtoupdate.Position - 1;
                                skillist.Add(itemtoupdate, itemtoupdate);
                            }
                        }
                    }
                }
            }

            //write result to file
            using (StreamWriter stream = new StreamWriter(AppRoot + "\\OutputFile.txt", false))
            {
                foreach (var circuit in circuits)
                {
                    var line = circuit.Key;
                    foreach (var juggler in circuit.Value.jugglers)
                    {
                        line += " " + juggler.JugglerId;
                        foreach (var preferredcircuit in juggler.PreferredCircuits)
                            line += " " + preferredcircuit.CircuitId + ":" + preferredcircuit.Skill.ToString();
                        line += ",";
                    }
                    line = line.Substring(0, line.Length - 1);
                    stream.WriteLine(line);
                    // looking for this?
                    if (circuit.Key == "C1970")
                    {
                        var jugglersum = 0;
                        foreach (var juggler in circuit.Value.jugglers)
                        {
                            jugglersum += int.Parse(juggler.JugglerId.Substring(1));
                        }
                        Console.WriteLine(line);
                        Console.WriteLine("Juggler Sum " + jugglersum.ToString());
                    }
                }
            }
            stopwatch.Stop();
            var elapsed_time = stopwatch.ElapsedMilliseconds;
            Console.WriteLine();
            Console.WriteLine(String.Format("Elapsed seconds {0}",TimeSpan.FromMilliseconds(elapsed_time).TotalSeconds.ToString()));

            Console.ReadKey();
        }

        public class Circuit
        {
            public string CircuitId { get; set; }
            public int H { get; set; }
            public int E { get; set; }
            public int P { get; set; }

            public List<Juggler> jugglers { get; set; }
        }

        public class Juggler
        {
            public string JugglerId { get; set; }
            public int H { get; set; }
            public int E { get; set; }
            public int P { get; set; }

            public List<JugglerSkill> PreferredCircuits { get; set; }
        }

        public class JugglerSkill
        {
            public int Position { get; set; }
            public int Skill { get; set; }
            public string JugglerId { get; set; }
            public string CircuitId { get; set; }
        }
    }
}